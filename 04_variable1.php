<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>variable</title>
    </head>
    <body>
        <?php
        $nombre="Miguel";
        $nombres = [
            "Eva",
            "Jose",
            "Ana",
            "Luis"
        ];
        ?>
        <div>
            <?= $nombre ?>
        </div>
        
        <?php
        foreach ($nombres as $k => $v) {
            echo "<div>$v</div>";
        }
        ?>
        
    </body>
</html>
