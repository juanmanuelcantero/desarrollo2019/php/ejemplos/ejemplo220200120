<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>6</title>
    </head>
    <body>
        <?php
        $enlaces = [
            "uno"=>"Home",
            "dos"=>"Contacto",
            "tres"=>"Conocenos"
        ]
        ?>
        <ul>
            <?php
                    foreach ($enlaces as $enlace => $etiqueta){
                        require 'li.php';                        
                    }
            ?>
        </ul>
        
    </body>
</html>
